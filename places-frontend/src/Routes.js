import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Places from "./containers/Places/Places";
import NewPlace from "./containers/NewPlace/NewPlace";
import SinglePlace from "./containers/SinglePlace/SinglePlace";

const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = ({user}) => (
    <Switch>
        <Route path="/" exact component={Places}/>
        <ProtectedRoute
            isAllowed={user && user.role === 'user'}
            path="/places/new"
            exact
            component={NewPlace}
        />
        <Route path="/register" exact component={Register}/>
        <Route path="/login" exact component={Login}/>
        <Route path="/place/:id" exact component={SinglePlace}/>
    </Switch>
);

export default Routes;
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from 'react-bootstrap';

import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {sendPhoto} from "../../store/actions/places";

class NewPhoto extends Component {
    sendPhoto = photoData => {
        this.props.onSendPhoto(photoData, this.props.placeId);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Upload new photo</PageHeader>
                <PhotoForm
                    onSubmit={this.sendPhoto}
                    placeId={this.props.placeId}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onSendPhoto: (photoData, placeId) => {
        return dispatch(sendPhoto(photoData, placeId))
    }
});

export default connect(null, mapDispatchToProps)(NewPhoto);
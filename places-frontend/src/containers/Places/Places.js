import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, PageHeader} from "react-bootstrap";

import {Link} from "react-router-dom";

import PlaceListItem from "../../components/PlaceListItem/PlaceListItem";
import {deletePlace, fetchPlaces} from "../../store/actions/places";

class Places extends Component {
    componentDidMount() {
        this.props.onFetchPlaces();
    }

    deletePlace = placeId => {
        this.props.onDeletePlace(placeId)
    };

    render() {
        console.log(this.props.places);
        return (
            <Fragment>
                <PageHeader>
                    All places
                    {this.props.user && this.props.user.role === 'user' &&
                    <Link to="/places/new">
                        <Button bsStyle="primary" className="pull-right">
                            Add new place
                        </Button>
                    </Link>
                    }
                </PageHeader>

                {this.props.places.map(place => (
                    <PlaceListItem
                        key={place._id}
                        placeId={place._id}
                        title={place.title}
                        image={place.image}
                        userId={place.author._id}
                        delete={() => this.deletePlace(place._id)}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    places: state.places.places,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchPlaces: () => dispatch(fetchPlaces()),
    onDeletePlace: id => dispatch(deletePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Places);
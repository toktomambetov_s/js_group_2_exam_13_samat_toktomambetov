import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Image, PageHeader} from "react-bootstrap";

import config from '../../config';

import {fetchSinglePlace} from "../../store/actions/places";

class Gallery extends Component {
    componentDidMount() {
        this.props.onFetchSinglePlace(this.props.placeId);
    }

    render() {
        console.log(this.props.singlePlace.photos);
        return (
            <Fragment>
                <PageHeader>
                    Gallery
                </PageHeader>

                {/*{this.props.singlePlace.photos.map(photo => (*/}
                    {/*<Image*/}
                        {/*style={{width: '100px', marginRight: '10px'}}*/}
                        {/*src={config.apiUrl + '/uploads/' + photo.photo}*/}
                        {/*thumbnail*/}
                    {/*/>*/}
                {/*))}*/}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    singlePlace: state.places.singlePlace,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchSinglePlace: id => dispatch(fetchSinglePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from 'react-bootstrap';

import ReviewForm from "../../components/ReviewForm/ReviewForm";
import {sendReview} from "../../store/actions/reviews";

class NewReview extends Component {
    sendReview = reviewData => {
        this.props.onSendReview(reviewData, this.props.placeId);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Add review</PageHeader>
                <ReviewForm
                    onSubmit={this.sendReview}
                    placeId={this.props.placeId}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onSendReview: (reviewData, placeId) => {
        return dispatch(sendReview(reviewData, placeId))
    }
});

export default connect(null, mapDispatchToProps)(NewReview);
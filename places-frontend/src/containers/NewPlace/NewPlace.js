import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from 'react-bootstrap';

import PlaceForm from "../../components/PlaceForm/PlaceForm";
import {createPlace} from "../../store/actions/places";

class NewPlace extends Component {
    createPlace = placeData => {
        this.props.onPlaceCreated(placeData);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Add new place</PageHeader>
                <PlaceForm
                    onSubmit={this.createPlace}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onPlaceCreated: placeData => {
        return dispatch(createPlace(placeData))
    }
});

export default connect(null, mapDispatchToProps)(NewPlace);
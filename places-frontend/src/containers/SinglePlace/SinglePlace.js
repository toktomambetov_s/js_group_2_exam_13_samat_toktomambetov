import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Image} from "react-bootstrap";

import config from "../../config";
import {fetchSinglePlace} from "../../store/actions/places";
import NewReview from "../NewReview/NewReview";
import Reviews from "../Reviews/Reviews";
import NewPhoto from "../NewPhoto/NewPhoto";
import Gallery from "../Gallery/Gallery";

class SinglePlace extends Component {
    componentDidMount() {
        this.props.onFetchSinglePlace(this.props.match.params.id);
    }

    render() {
        console.log(this.props.singlePlace);
        return (
            <Fragment>
                <Image
                    style={{width: '500px', marginRight: '10px'}}
                    src={config.apiUrl + '/uploads/' + this.props.singlePlace.image}
                    thumbnail
                />
                <h2>{this.props.singlePlace.title}</h2>
                <p style={{'fontSize': '20px'}}>{this.props.singlePlace.description}</p>

                <Gallery placeId={this.props.match.params.id} photos={this.props.singlePlace.photos}/>

                <Reviews placeId={this.props.match.params.id}/>

                {this.props.user &&
                <NewReview placeId={this.props.match.params.id}/>
                }

                <NewPhoto placeId={this.props.match.params.id}/>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    singlePlace: state.places.singlePlace,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchSinglePlace: id => dispatch(fetchSinglePlace(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePlace);

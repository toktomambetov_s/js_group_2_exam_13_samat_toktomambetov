import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ReviewListItem from "../../components/ReviewListItem/ReviewListItem";

import {fetchReviews} from "../../store/actions/reviews";

class Reviews extends Component {
    componentDidMount() {
        this.props.onFetchReviews(this.props.placeId);
    }

    render() {
        console.log(this.props.reviews);
        return (
            <Fragment>
                <PageHeader>
                    Reviews
                </PageHeader>

                {this.props.reviews.map(review => (
                    <ReviewListItem
                        key={review._id}
                        date={review.date}
                        review={review.review}
                        foodQuality={review.foodQuality}
                        serviceQuality={review.serviceQuality}
                        interior={review.interior}
                        userId={review.author._id}
                        displayName={review.author.displayName}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    reviews: state.reviews.reviews
});

const mapDispatchToProps = dispatch => ({
    onFetchReviews: id => dispatch(fetchReviews(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
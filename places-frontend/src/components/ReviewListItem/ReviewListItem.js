import React from 'react';
import {connect} from 'react-redux';
import {Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import Rating from 'react-rating';
import PropTypes from 'prop-types';

import greyStar from '../../assets/images/grey-star.jpg.png';
import yellowStar from '../../assets/images/yellow-star.png';

const ReviewListItem = props => {
    return (
        <Panel>
            <Panel.Body>
                <h3>On {props.date}, <Link to={'/user/' + props.userId}>{props.displayName}</Link> said: </h3>

                <p style={{'fontSize': '20px'}}>{props.review}</p>

                <div style={{'fontWeight': 'bold'}}>Quality of food: <Rating
                    initialRating={props.foodQuality}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    readonly={true}
                />
                </div>

                <div style={{'fontWeight': 'bold'}}>Service quality: <Rating
                    initialRating={props.serviceQuality}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    readonly={true}
                />
                </div>

                <div style={{'fontWeight': 'bold'}}>Interior: <Rating
                    initialRating={props.interior}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    readonly={true}
                />
                </div>

            </Panel.Body>
        </Panel>
    );
};

ReviewListItem.propTypes = {
    date: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    foodQuality: PropTypes.number.isRequired,
    serviceQuality: PropTypes.number.isRequired,
    interior: PropTypes.number.isRequired,
    displayName: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps)(ReviewListItem);
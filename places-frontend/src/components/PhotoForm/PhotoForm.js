import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {connect} from 'react-redux';

import FormElement from "../UI/Form/FormElement";

class PlaceForm extends Component {
    state = {
        place: this.props.placeId,
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    fieldHasError = fieldName => {
        return this.props.error && this.props.error.errors[fieldName];
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>

                <FormElement
                    propertyName="image"
                    title="Photo"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                    error={this.fieldHasError('image') && this.props.error.errors.image.message}
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Upload</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.places.error
});

export default connect(mapStateToProps)(PlaceForm);

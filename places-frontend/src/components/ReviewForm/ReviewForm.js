import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import Rating from 'react-rating';

import FormElement from "../UI/Form/FormElement";
import greyStar from '../../assets/images/grey-star.jpg.png';
import yellowStar from '../../assets/images/yellow-star.png';

class ReviewForm extends Component {
    state = {
        review: '',
        foodQuality: null,
        serviceQuality: null,
        interior: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const ratingData = {
            review: this.state.review,
            foodQuality: this.state.foodQuality,
            serviceQuality: this.state.serviceQuality,
            interior: this.state.interior,
            placeId: this.props.placeId
        };

        // const formData = new FormData();
        // Object.keys(this.state).forEach(key => {
        //     formData.append(key, this.state[key]);
        // });

        this.props.onSubmit(ratingData);

        this.setState({
            review: '',
            foodQuality: null,
            serviceQuality: null,
            interior: null
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    setFoodQualityRating = (value) => {
        this.setState({foodQuality: value})
    };

    setServiceQualityRating = (value) => {
        this.setState({serviceQuality: value})
    };

    setInteriorRating = (value) => {
        this.setState({interior: value})
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="review"
                    title="Review"
                    type="textarea"
                    value={this.state.review}
                    changeHandler={this.inputChangeHandler}
                    required
                />

                <div style={{'fontWeight': 'bold'}}>Quality of food: <Rating
                    onChange={(value) => {
                        this.setFoodQualityRating(value)
                    }}
                    initialRating={this.state.foodQuality}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    fractions={2}
                />
                </div>

                <div style={{'fontWeight': 'bold'}}>Service quality: <Rating
                    onChange={(value) => {
                        this.setServiceQualityRating(value)
                    }}
                    initialRating={this.state.serviceQuality}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    fractions={2}
                />
                </div>

                <div style={{'fontWeight': 'bold'}}>Interior: <Rating
                    onChange={(value) => {
                        this.setInteriorRating(value)
                    }}
                    initialRating={this.state.interior}
                    emptySymbol={<img src={greyStar} alt=""/>}
                    fullSymbol={<img src={yellowStar} alt=""/>}
                    fractions={2}
                />
                </div>

                <FormGroup style={{'marginTop': '20px'}}>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Submit review</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default ReviewForm;

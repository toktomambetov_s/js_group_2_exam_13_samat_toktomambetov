import React from 'react';
import {connect} from 'react-redux';
import {Image, Panel, Button} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../config';

const PlaceListItem = props => {
    return (
        <Panel>
            <Panel.Body>
                <Link to={'/place/' + props.placeId}>
                    <Image
                        style={{width: '100px', marginRight: '10px'}}
                        src={config.apiUrl + '/uploads/' + props.image}
                        thumbnail
                    />
                </Link>

                <Link to={'/place/' + props.placeId}>
                    {props.title}
                </Link>

                {props.user && props.user.role === 'admin' &&
                <Button onClick={props.delete} bsStyle="primary" style={{marginLeft: '20px'}}>
                    Delete
                </Button>
                }

            </Panel.Body>
        </Panel>
    );
};

PlaceListItem.propTypes = {
    placeId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    user: state.users.user
});

export default connect(mapStateToProps)(PlaceListItem);
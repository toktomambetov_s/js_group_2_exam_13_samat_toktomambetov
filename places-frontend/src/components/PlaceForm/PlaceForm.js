import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {connect} from 'react-redux';

import FormElement from "../UI/Form/FormElement";

class PlaceForm extends Component {
    state = {
        title: '',
        description: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    fieldHasError = fieldName => {
        return this.props.error && this.props.error.errors[fieldName];
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text"
                    value={this.state.title}
                    changeHandler={this.inputChangeHandler}
                    autoComplete="new-title"
                    error={this.fieldHasError('title') && this.props.error.errors.title.message}
                />

                <FormElement
                    propertyName="description"
                    title="Description"
                    type="text"
                    value={this.state.description}
                    changeHandler={this.inputChangeHandler}
                    error={this.fieldHasError('description') && this.props.error.errors.description.message}
                />

                <FormElement
                    propertyName="image"
                    title="Main photo"
                    type="file"
                    changeHandler={this.fileChangeHandler}
                    error={this.fieldHasError('image') && this.props.error.errors.image.message}
                />

                <FormElement
                    propertyName="agreement"
                    title="By submitting this form, you agree that
                    the following information will be submitted to the public domain, and
                    administrators of this site will have full
                    control over the said information"
                    type="checkbox"
                    required
                />

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Submit new place</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.places.error
});

export default connect(mapStateToProps)(PlaceForm);

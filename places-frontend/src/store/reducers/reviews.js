import {FETCH_REVIEWS_SUCCESS, SEND_REVIEW_FAILURE} from "../actions/actionTypes";

const initialState = {
    reviews: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REVIEWS_SUCCESS:
            return {...state, reviews: action.reviews};
        case SEND_REVIEW_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default reducer;
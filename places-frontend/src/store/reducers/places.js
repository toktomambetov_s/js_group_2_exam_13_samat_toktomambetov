import {CREATE_PLACE_FAILURE, FETCH_PLACES_SUCCESS, FETCH_SINGLE_PLACE_SUCCESS} from "../actions/actionTypes";

const initialState = {
    places: [],
    singlePlace: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PLACES_SUCCESS:
            return {...state, places: action.places};
        case CREATE_PLACE_FAILURE:
            return {...state, error: action.error};
        case FETCH_SINGLE_PLACE_SUCCESS:
            return {...state, singlePlace: action.singlePlace};
        default:
            return state;
    }
};

export default reducer;
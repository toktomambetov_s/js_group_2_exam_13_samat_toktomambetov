import axios from '../../axios-api'
import {NotificationManager} from 'react-notifications';

import {FETCH_REVIEWS_SUCCESS, SEND_REVIEW_FAILURE, SEND_REVIEW_SUCCESS} from "./actionTypes";

export const fetchReviewsSuccess = reviews => {
    return {type: FETCH_REVIEWS_SUCCESS, reviews};
};

export const fetchReviews = id => {
    return dispatch => {
        axios.get('/reviews/' + id).then(
            response => dispatch(fetchReviewsSuccess(response.data))
        )
    }
};

export const sendReviewSuccess = () => {
    return {type: SEND_REVIEW_SUCCESS}
};

export const sendReviewFailure = (error) => {
    return {type: SEND_REVIEW_FAILURE, error}
};

export const sendReview = (reviewData, id) => {
    return dispatch => {
        axios.post('/reviews/publish', reviewData).then(
            response => {
                dispatch(sendReviewSuccess());
                dispatch(fetchReviews(id));
            }, error => {
                dispatch(sendReviewFailure(error.response.data));
                NotificationManager.error('Error', 'Failed to send data');
            }
        )
    }
};
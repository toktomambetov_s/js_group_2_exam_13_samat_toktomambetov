import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {NotificationManager} from 'react-notifications';
import {
    CREATE_PLACE_FAILURE, CREATE_PLACE_SUCCESS, DELETE_PLACE_SUCCESS, FETCH_PLACES_SUCCESS,
    FETCH_SINGLE_PLACE_SUCCESS, SEND_PHOTO_SUCCESS
} from "./actionTypes";

export const fetchPlacesSuccess = places => {
    return {type: FETCH_PLACES_SUCCESS, places};
};

export const fetchPlaces = () => {
    return dispatch => {
        axios.get('/places').then(
            response => dispatch(fetchPlacesSuccess(response.data))
        );
    }
};

export const createPlaceSuccess = () => {
    return {type: CREATE_PLACE_SUCCESS};
};

export const createPlaceFailure = (error) => {
    return {type: CREATE_PLACE_FAILURE, error}
};

export const createPlace = placeData => {
    return dispatch => {
        return axios.post('/places/publish', placeData).then(
            response => {
                dispatch(createPlaceSuccess());
                dispatch(push('/'));
            }, error => {
                dispatch(createPlaceFailure(error.response.data));
                NotificationManager.error('Error', 'Failed to send data');
            }
        );
    };
};

export const sendPhotoSuccess = () => {
    return {type: SEND_PHOTO_SUCCESS};
};

export const sendPhoto = (photoData, id) => {
    return dispatch => {
        return axios.post('/photos/publish/' + id, photoData).then(
            response => {
                dispatch(sendPhotoSuccess());
                dispatch(fetchSinglePlace(id));
            }
        )
    }
};

export const fetchSinglePlaceSuccess = singlePlace => {
    return {type: FETCH_SINGLE_PLACE_SUCCESS, singlePlace}
};

export const fetchSinglePlace = id => {
    return dispatch => {
        axios.get('/places/' + id).then(
            response => dispatch(fetchSinglePlaceSuccess(response.data))
        )
    }
};

export const deletePlaceSuccess = () => {
    return {type: DELETE_PLACE_SUCCESS}
};

export const deletePlace = id => {
    return dispatch => {
        return axios.delete('/places/' + id).then(
            response => {
                dispatch(deletePlaceSuccess());
                dispatch(fetchPlaces());
            }
        )
    }
};
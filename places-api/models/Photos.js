const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PhotoSchema = new Schema({
    photo: {
        type: String,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    place: {
        type: Schema.Types.ObjectId,
        ref: 'Place'
    }
});

const Photo = mongoose.model('Photo', PhotoSchema);

module.exports = Photo;
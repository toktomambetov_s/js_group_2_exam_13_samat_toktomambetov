const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    author: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    placeId: {
        type: Schema.ObjectId,
        ref: 'Place',
        required: true
    },
    foodQuality: {
        type: Number,
        required: true
    },
    serviceQuality: {
        type: Number,
        required: true
    },
    interior: {
        type: Number,
        required: true
    },
    review: {
        type: String,
        required: true
    }
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;
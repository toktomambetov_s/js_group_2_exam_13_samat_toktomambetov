const express = require('express');
const Review = require('../models/Review');
const auth = require('../middleware/auth');

const createRouter = () => {
    const router = express.Router();

    router.get('/:id', (req, res) => {
        const id = req.params.id;

        Review.find({placeId: id}).populate('author').populate('placeId')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/publish', auth, (req, res) => {
        let reviewData = req.body;

        reviewData.author = req.user._id;

        const review = new Review(reviewData);

        review.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;
const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Place = require('../models/Place');
const Photo = require('../models/Photos');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.post('/publish/:id', [auth, upload.single('image')], async (req, res) => {
        let photoData = req.body;

        if (req.file) {
            photoData.photo = req.file.filename;
        } else {
            photoData.photo = null;
        }

        photoData.author = req.user._id;

        const photo = new Photo(photoData);
        const savedPhoto = await photo.save();

        const id = req.params.id;
        const place = await Place.findOne({_id: id});
        place.photos.push(savedPhoto._id);
        await place.save();

        return status(200).send({place});
    });

    return router;
};

module.exports = createRouter;
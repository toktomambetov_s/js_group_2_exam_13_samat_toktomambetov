const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Place = require('../models/Place');
const auth = require('../middleware/auth');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Place.find().populate('author').populate('photos')
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    router.post('/publish', [auth, upload.single('image')], (req, res) => {
        let placeData = req.body;

        if (req.file) {
            placeData.image = req.file.filename;
        } else {
            placeData.image = null;
        }

        placeData.author = req.user._id;

        const place = new Place(placeData);

        place.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;

        Place.findOne({_id: id}).populate('photos')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;

        Place.deleteOne({_id: id})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(400).send(error);
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;
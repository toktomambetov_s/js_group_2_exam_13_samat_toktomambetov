const mongoose = require('mongoose');
const config = require('./config');

const Place = require('./models/Place');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('places');
        await db.dropCollection('users');
        await db.dropCollection('reviews');
        await db.dropCollection('photos');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user, admin] = await User.create({
        username: 'user',
        password: '123',
        role: 'user',
        displayName: 'John Doe'
    }, {
        username: 'admin',
        password: '123',
        role: 'admin',
        displayName: 'Jack Daniels'
    });

    await Place.create({
        title: 'Afterlife',
        description: 'A nightclub located on Omega, featuring exotic Asari dancers, automated and live barmen, wide variety of all-species compatible drinks.',
        image: 'afterlife.jpg',
        author: user._id
    }, {
        title: 'Opium',
        description: 'Located in one of the most privileged places of the Paseo Marítimo de la Barceloneta, with a large terrace that allows the best view of the Mediterranean Sea, Opium Barcelona is the place where the most renowned DJs from around the world meet, ready to to offer the best funniest shows of electronic music, becoming the reference club of the Catalan night scene.',
        image: 'opium.jpg',
        author: user._id
    });

    db.close();
});